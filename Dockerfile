#FROM golang:1.10.2 as builder
#
#WORKDIR /go/src/go-ci
#
#COPY . .
#
#RUN go get -u github.com/labstack/echo/...
#RUN ls
#RUN go build .
#
#
#FROM alpine:latest
#RUN apk --no-cache add ca-certificates
#
#COPY --from=builder /go/src/go-ci .
#
#RUN /go-ci
FROM golang:latest

RUN go get -u github.com/labstack/echo/...

WORKDIR /go/src/go-ci
COPY . .

RUN go install -v

RUN go build .

ENTRYPOINT ["./go-ci"]
