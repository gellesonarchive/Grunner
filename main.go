package main

import (
	"github.com/labstack/echo"
	"go-ci/runners"
	"net/http"
)

func hello(c echo.Context) error {
	return c.JSON(http.StatusOK, "Hello world")
}

func main() {
	server := echo.New()
	server.GET("/hello", hello)
	admin := server.Group("admin")
	runners.GROUPS(admin)
	server.Start(":80")
}
